! Title: Atombomba a kamuoldalakra
! Description: A kamuoldalak teljes és totális felszámolása
! Version: 0.0.0.0.0.0.0.0.0.0.0.0.1


fontostudni.club
fontostudni.club/*
||fontostudni.club^$domain=*,important
fontostudni.club##html
*$domain=fontostudni.club,important

emberitemak.club
emberitemak.club/*
||emberitemak.club^$domain=*,important
emberitemak.club##html
*$domain=emberitemak.club,important

erdekestemak.info
erdekestemak.info/*
||erdekestemak.info^$domain=*,important
erdekestemak.info##html
*$domain=erdekestemak.info,important

erdekeshirek.club
erdekeshirek.club/*
||erdekeshirek.club^$domain=*,important
erdekeshirek.club##html
*$domain=erdekeshirek.club,important
